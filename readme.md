# Zammad install

This is my way of installing zammad using ansible.

I didn't like the other roles that i found in galaxy, or other places.

## Usage

- Create your TLS certs and place them in the certs folder.
  - `{{nginx_hostname}}.crt`
  - `{{nginx_hostname}}.key`
- Copy the template inventory
  - `cp template.inventory.yml inventory.yml`
- Update the inventory
  - at a minimum update the `hosts` line and the `nginx-hostname` var
- Run the playbook
  - `ansible-playbook -i inventory.yml install.yml`

### Notes

- If you don't have a user on the host that has passwordless sudo run this instead
  - `ansible-playbook -i inventory.yml install.yml -K`
- You can also change those settings in the `inventory.yml`